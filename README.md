# [Muse Vue Ant Design Dashboard](http://demos.creative-tim.com/muse-vue-ant-design-dashboard?ref=readme-sud) 

Muse - Vue Ant Design Dashboard是一个漂亮的Ant Design Vue管理仪表板，有大量的组件，设计得看起来很漂亮，很有条理
专为那些喜欢大胆的元素和美丽的网站。由数百个元素，设计块和完全编码的页面，软UI仪表板是准备帮助你创建惊人的网站和网络应用程序。
我们为Sign In、Profile等页面创建了许多示例。只要选择一个基本设计，插图或封面，你是好的去!

**Fully Coded Elements**

Muse - Vue Ant设计仪表板由70多个前端独立元素组成，如按钮、输入、导航条、导航标签、卡片或提醒，让你自由选择和组合。所有组件都可以采用不同的颜色，您可以使用SASS文件和类轻松地修改这些颜色。您将节省大量时间从原型开发到全功能代码，因为所有元素都实现了。

浏览 <a href="https://www.creative-tim.com/muse-vue-ant-design-dashboard/documentation/" target="_blank">所有组件</a>.

**Documentation built by Developers**

每个元素都在一个非常复杂的文档中很好地呈现出来。
你可以在这里阅读 <a href="https://www.creative-tim.com/muse-vue-ant-design-dashboard/documentation/" target="_blank">更多文档</a>.

**Example Pages**

如果你想获得灵感或者只是直接向客户展示一些东西，您可以使用我们预先构建的示例页面开始您的开发。你将能够快速设置您的web项目的基本结构。
View <a href="https://www.creative-tim.com/muse-vue-ant-design-dashboard" target="_blank">example pages here</a>.

**帮助链接**

- View <a href="https://github.com/creativetimofficial/muse-vue-ant-design-dashboard" target="_blank">Github Repository</a>

- Check <a href="https://www.creative-tim.com/faq" target="_blank">FAQ Page</a>

**Special thanks**

During the development of this dashboard, we have used many existing resources from awesome developers. We want to thank them for providing their tools open source:

- [Ant Design Vue](https://www.antdv.com/docs/vue/introduce/)- An enterprise-class UI design language for web applications

Let us know your thoughts below. And good luck with development!

## Table of Contents

* [Versions](#versions)
* [Demo](#demo)
* [Quick Start](#quick-start)
* [Documentation](#documentation)
* [File Structure](#file-structure)
* [Browser Support](#browser-support)
* [Resources](#resources)
* [Reporting Issues](#reporting-issues)
* [Technical Support or Questions](#technical-support-or-questions)
* [Licensing](#licensing)
* [Useful Links](#useful-links)


| HTML |
| --- |
| [![Muse Vue Ant Design Dashboard](https://s3.amazonaws.com/creativetim_bucket/products/494/thumb/opt_md_ant_thumbnail.jpg)](http://demos.creative-tim.com/muse-vue-ant-design-dashboard?ref=readme-sud)

## Demo

- [Profile page](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/profile?ref=readme-sud)
- [Sign in page](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/sign-in?ref=readme-sud)
- [Sign up page](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/sign-up?ref=readme-sud)

[View More](http://demos.creative-tim.com/muse-vue-ant-design-dashboard).


## Terminal Commands

1. 下载安装 [NodeJs](https://nodejs.org/en/download/).
2. 解压到一个目录并运行`npm install`来安装.

## Documentation
Muse Vue Ant设计仪表板的文档在我们的 [网站](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/documentation?ref=readme-sud)上.

### What's included

你会发现以下目录和文件:

```
muse-vue-ant-design-dashboard
├── LICENSE
├── README.md
├── babel.config.js
├── gulpfile.js
├── package.json
├── public
│   ├── imagesd
│   └── index.html
├── src
│   ├── App.vue
│   ├── assets
│   ├── components
│   │   ├── Cards
│   │   │   ├── CardAuthorTable.vue
│   │   │   ├── CardBarChart.vue
│   │   │   ├── CardBillingInfo.vue
│   │   │   ├── CardConversations.vue
│   │   │   ├── CardCredit.vue
│   │   │   ├── CardInfo.vue
│   │   │   ├── CardInfo2.vue
│   │   │   ├── CardInvoices.vue
│   │   │   ├── CardLineChart.vue
│   │   │   ├── CardOrderHistory.vue
│   │   │   ├── CardPaymentMethods.vue
│   │   │   ├── CardPlatformSettings.vue
│   │   │   ├── CardProfileInformation.vue
│   │   │   ├── CardProject.vue
│   │   │   ├── CardProjectTable.vue
│   │   │   ├── CardProjectTable2.vue
│   │   │   └── CardTransactions.vue
│   │   ├── Charts
│   │   │   ├── ChartBar.vue
│   │   │   └── ChartLine.vue
│   │   ├── Footers
│   │   │   ├── DashboardFooter.vue
│   │   │   └── DefaultFooter.vue
│   │   ├── Headers
│   │   │   ├── DashboardHeader.vue
│   │   │   └── DefaultHeader.vue
│   │   ├── Sidebars
│   │   │   ├── DashboardSettingsDrawer.vue
│   │   │   └── DashboardSidebar.vue
│   │   └── Widgets
│   │       ├── WidgetCounter.vue
│   │       └── WidgetSalary.vue
│   ├── layouts
│   │   ├── Dashboard.vue
│   │   ├── DashboardRTL.vue
│   │   └── Default.vue
│   ├── main.js
│   ├── plugins
│   │   └── click-away.js
│   ├── router
│   │   └── index.js
│   ├── scss
│   │   ├── app.scss
│   │   ├── base
│   │   │   ├── _override.scss
│   │   │   ├── _typography.scss
│   │   │   ├── _utilities.scss
│   │   │   └── _variables.scss
│   │   ├── components
│   │   │   ├── _avatar.scss
│   │   │   ├── _badge.scss
│   │   │   ├── _button.scss
│   │   │   ├── _card.scss
│   │   │   ├── _chart.scss
│   │   │   ├── _dropdown.scss
│   │   │   ├── _list.scss
│   │   │   ├── _progress.scss
│   │   │   ├── _settings-drawer.scss
│   │   │   ├── _space.scss
│   │   │   ├── _table.scss
│   │   │   ├── _tag.scss
│   │   │   ├── _timeline.scss
│   │   │   └── _widget.scss
│   │   ├── form
│   │   │   ├── _checkbox.scss
│   │   │   └── _input.scss
│   │   ├── layouts
│   │   │   ├── _dashboard-rtl.scss
│   │   │   ├── _dashboard.scss
│   │   │   └── _default.scss
│   │   └── pages
│   │       ├── _profile.scss
│   │       ├── _sign-in.scss
│   │       └── _sign-up.scss
│   └── views
│       ├── 404.vue
│       ├── Billing.vue
│       ├── Dashboard.vue
│       ├── Layout.vue
│       ├── Profile.vue
│       ├── RTL.vue
│       ├── Sign-In.vue
│       ├── Sign-Up.vue
│       └── Tables.vue
└── vue.config.js
```



## Resources
- [Live Preview](https://demos.creative-tim.com/muse-vue-ant-design-dashboard?ref=readme-sud)
- [Download Page](https://www.creative-tim.com/product/muse-vue-ant-design-dashboard?ref=readme-sud)
- Documentation is [here](https://demos.creative-tim.com/muse-vue-ant-design-dashboard/documentation?ref=readme-sud)
- [License Agreement](https://www.creative-tim.com/license?ref=readme-sud)
- [Support](https://www.creative-tim.com/contact-us?ref=readme-sud)
- Issues: [Github Issues Page](https://github.com/creativetimofficial/muse-vue-ant-design-dashboard/issues)


## Licensing

- Copyright 2021 [Creative Tim](https://www.creative-tim.com?ref=readme-sud)
- Creative Tim [license](https://www.creative-tim.com/license?ref=readme-sud)

## Useful Links

- [More products](https://www.creative-tim.com/templates?ref=readme-sud) from Creative Tim

- [Tutorials](https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w)

- [Freebies](https://www.creative-tim.com/bootstrap-themes/free?ref=readme-sud) from Creative Tim

- [Affiliate Program](https://www.creative-tim.com/affiliates/new?ref=readme-sud) (earn money)
